const express = require('express');
const app = express();
const userController = require('./controllers/userController.js');
// const cors = require('cors')


app.all('/*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
});

// app.use(cors());
app.use('/api', userController);

app.listen(8000);


