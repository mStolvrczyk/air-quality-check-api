module.exports = {
    getData: function ({data}) {
        return data;
    },
    stationsFilter: function ({id, stationName, gegrLat, gegrLon, city}) {
        return {
            id,
            stationName,
            gegrLat,
            gegrLon,
            city: city ? city.name: ''
        };
    },
    sensorsFilter: function ({id, param}) {
        return {
            id,
            param : param ? param.paramName : '',
            paramTwo : param ? param.paramFormula : ''
        }
    }
};

